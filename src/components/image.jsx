import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

const Image = props => {
  const { name } = props;

  const data = useStaticQuery(graphql`
    query {
      image1: file(relativePath: { eq: "image-1.png" }) {
        childImageSharp {
          fluid(pngQuality: 100, maxWidth: 600) {
            ...GatsbyImageSharpFluid
            ...GatsbyImageSharpFluidLimitPresentationSize
          }
        }
      }
    }
  `);

  return <Img fluid={data[name].childImageSharp.fluid} {...props} />;
};

export default Image;
