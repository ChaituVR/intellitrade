import React from 'react';
import { makeStyles, Typography, Container, Grid } from '@material-ui/core';

import Image from '../assets/images/image-2.svg';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#e9edee',
    padding: theme.spacing(13, 0),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(6, 0),
    },
  },
  textContainer: {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
    '& > *': {
      marginBottom: theme.spacing(2),
    },
  },
  item1: {
    [theme.breakpoints.down('sm')]: {
      order: 2,
    },
  },
  item2: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
    },
  },
  imageWrapper: {
    display: 'flex',
  },
  image: {
    marginLeft: 'auto',
    height: '100%',
  },
}));

export default function SectionC() {
  const classes = useStyles();

  return (
    <section id="about" className={classes.root}>
      <Container>
        <Grid container spacing={4}>
          <Grid container item xs={12} md={5} className={classes.item1}>
            <div className={classes.textContainer}>
              <Typography variant="h2">About Us</Typography>
              <Typography variant="subtitle1">
                IntelliTrade provides actionable low risk/high return trade signals in an easy to follow format. The
                research team at IntelliTrade uses a process that has been developed from decades of professional
                trading experience.
              </Typography>
              <Typography variant="subtitle1">
                Unlike Wall Street where the goal is to make themselves money off the customer’s back, there is no
                bullshit with IntelliTrade.
              </Typography>
              <Typography variant="subtitle1">
                We have a strict policy of not front running trades or providing trade signals to higher paying
                customers first.
              </Typography>
              <Typography variant="subtitle1">All customers are treated equal at IntelliTrade.</Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={7} className={classes.item2}>
            <div className={classes.imageWrapper}>
              <Image className={classes.image} />
            </div>
          </Grid>
        </Grid>
      </Container>
    </section>
  );
}
