import React from 'react';
import clsx from 'clsx';
import { makeStyles, Typography, Container, Grid, Hidden, Link } from '@material-ui/core';

import Logo from '../assets/images/logo-2.svg';
import MailIcon from '../assets/icons/mail.svg';
import TwitterIcon from '../assets/icons/twitter.svg';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#e9edee',
    padding: theme.spacing(11, 0),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(6, 0),
    },
  },
  container: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      textAlign: 'center',
    },
  },
  col1: {
    flexGrow: 1,
    '& > *': {
      marginBottom: theme.spacing(4),
    },
    [theme.breakpoints.down('sm')]: {
      '& > *': {
        marginBottom: theme.spacing(2),
      },
    },
  },
  col2: {
    '& > *': {
      marginBottom: theme.spacing(4),
    },
    [theme.breakpoints.down('sm')]: {
      '& > *': {
        marginBottom: theme.spacing(2),
      },
    },
  },
  iconWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center',
    },
  },
}));

export default function SectionC() {
  const classes = useStyles();

  return (
    <section className={classes.root}>
      <Container>
        <div className={classes.container}>
          <div className={classes.col1}>
            <div>
              <Logo />
            </div>
            <div>
              <Typography variant="h6">© 2020 Intellitrade. All rights reserved.</Typography>
            </div>
          </div>
          <div className={classes.col2}>
            <div className={classes.iconWrapper}>
              <Link href="#">
                <MailIcon />
              </Link>
              <Link href="#">
                <TwitterIcon />
              </Link>
            </div>
            <div>
              <Typography variant="h6">Disclaimer can go in here.</Typography>
            </div>
          </div>
        </div>
        <div className={classes.container}></div>
      </Container>
    </section>
  );
}
