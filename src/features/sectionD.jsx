import React from 'react';
import clsx from 'clsx';
import { makeStyles, Typography, Container, Grid, Hidden } from '@material-ui/core';

import PriceIcon from '../assets/icons/price.svg';
import RefundIcon from '../assets/icons/refund.svg';
import OperatorIcon from '../assets/icons/operator.svg';
import DownloadIcon from '../assets/icons/download.svg';
import PasswordIcon from '../assets/icons/password.svg';

import Card from '../components/card';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#f0f5f6',
    padding: theme.spacing(13, 0),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(6, 0),
    },
  },
  textSection: {
    marginBottom: theme.spacing(4),
  },
}));

export default function SectionC() {
  const classes = useStyles();

  return (
    <section id="faq" className={classes.root}>
      <Container>
        <div className={classes.textSection}>
          <Typography variant="h2" align="center">
            FAQ
          </Typography>
          <Typography variant="h4" align="center">
            Please select a question.
          </Typography>
        </div>

        <Grid container spacing={3}>
          <Grid item xs={12} md={4}>
            <Card>
              <PriceIcon className="icon" />
              <Typography variant="h4" align="center">
                What is the pricing of IntelliTrade?
              </Typography>
              <Typography variant="h6" align="center">
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                provident.
              </Typography>
            </Card>
          </Grid>
          <Grid item xs={12} md={4}>
            <Card>
              <RefundIcon className="icon" />
              <Typography variant="h4" align="center">
                Is my purchase refundable?
              </Typography>
              <Typography variant="h6" align="center">
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                provident.
              </Typography>
            </Card>
          </Grid>
          <Grid item xs={12} md={4}>
            <Card>
              <OperatorIcon className="icon" />
              <Typography variant="h4" align="center">
                How can I contact the support team?
              </Typography>
              <Typography variant="h6" align="center">
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                provident.
              </Typography>
            </Card>
          </Grid>
          <Hidden smDown>
            <Grid item xs={12} md={2} />
          </Hidden>
          <Grid item xs={12} md={4}>
            <Card>
              <DownloadIcon className="icon" />
              <Typography variant="h4" align="center">
                Where can I download IntelliTrade?
              </Typography>
              <Typography variant="h6" align="center">
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                provident.
              </Typography>
            </Card>
          </Grid>
          <Grid item xs={12} md={4}>
            <Card>
              <PasswordIcon className="icon" />
              <Typography variant="h4" align="center">
                Where can I change my password?
              </Typography>
              <Typography variant="h6" align="center">
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                provident.
              </Typography>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </section>
  );
}
