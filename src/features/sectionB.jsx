import React from 'react';
import { makeStyles, useMediaQuery, Typography, Container, Grid } from '@material-ui/core';

import ContractIcon from '../assets/icons/contract.svg';
import IdeaIcon from '../assets/icons/idea.svg';
import GiftIcon from '../assets/icons/gift.svg';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#f0f5f6',
    padding: theme.spacing(13, 0),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(6, 0),
    },
  },
  textContainer: {
    marginTop: theme.spacing(2),
  },
}));

export default function SectionB() {
  const classes = useStyles();
  const matches = useMediaQuery(theme => theme.breakpoints.down('sm'));

  return (
    <section id="features" className={classes.root}>
      <Container>
        <Typography variant="h2">Features</Typography>
        <Typography variant="subtitle1">Short description could go here.</Typography>
        <Grid container spacing={matches ? 3 : 7} className={classes.textContainer}>
          <Grid item xs={12} md={4}>
            <ContractIcon />
            <Typography variant="h3" gutterBottom>
              Daily Briefing
            </Typography>
            <Typography variant="h4">
              The IntelliTrade daily briefing provides insight into the latest and most important market developments so
              that you are prepared for the trading day. The one-page briefing will be delivered every trading day
              before the market opens. The IntelliTrade team is very skilled at making the complex simpler.
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <IdeaIcon />
            <Typography variant="h3" gutterBottom>
              Experience
            </Typography>
            <Typography variant="h4">
              The team at IntelliTrade does not chase stocks, throw darts at a board, or pass on stock tips from chat
              rooms. We follow a strict research and risk management process to not only tell you when to enter a trade
              but when to exit as well.
            </Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            <GiftIcon />
            <Typography variant="h3" gutterBottom>
              Giving Back
            </Typography>
            <Typography variant="h4">
              IntelliTrade gives back 10% of all proceeds via interest free microloans to small business start-ups. The
              twist is all IntelliTrade customers get to vote on which businesses receive the loans. When loans are
              payed back the money goes back to the pool in order to help start more businesses.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </section>
  );
}
