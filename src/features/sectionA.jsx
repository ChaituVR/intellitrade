import React from 'react';
import { makeStyles, AppBar, Toolbar, Typography, IconButton, Button, Container, Grid, Box } from '@material-ui/core';

import Header from './header';
import Image from '../components/image';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: 'linear-gradient(180deg, #007ff5 0%, #0068c9 100%)',
  },
  container: {
    padding: theme.spacing(13, 0),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(5, 0),
    },
  },
  textContainer: {
    margin: 'auto',
    '& > *': {
      marginBottom: theme.spacing(5),
    },
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
  },
  button: {
    backgroundImage: 'linear-gradient(180deg, #00dd41 0%, #00b134 100%)',
    boxShadow: '0 5px 10px rgba(0, 0, 0, 0.15)',
    maxWidth: 347,
    height: 60,
    borderRadius: 8,
    marginBottom: theme.spacing(1),
  },
  image: {
    marginLeft: 'auto',
    [theme.breakpoints.down('sm')]: {
      marginRight: 'auto',
    },
  },
  item1: {
    [theme.breakpoints.down('sm')]: {
      order: 2,
    },
  },
  item2: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
    },
  },
}));

export default function SectionA() {
  const classes = useStyles();

  return (
    <section className={classes.root}>
      <Container>
        <Header />
        <div className={classes.container}>
          <Grid container>
            <Grid container item xs={12} md={5} className={classes.item1}>
              <div className={classes.textContainer}>
                <Typography variant="h1" color="textSecondary">
                  A short slogan or title can go here.
                </Typography>
                <Typography variant="h4" color="textSecondary">
                  Some short description or selling point can go here. The description you sent me was pretty long but I
                  think we can put it somewhere else where it’ll look better on the website.
                </Typography>
                <div>
                  <Button fullWidth className={classes.button}>
                    Start Your Free Month
                  </Button>
                  <Typography color="textSecondary" variant="subtitle1">
                    30 day free trial then $19.99 per month.
                  </Typography>
                </div>
              </div>
            </Grid>
            <Grid item xs={12} md={7} className={classes.item2}>
              <div>
                <Image className={classes.image} name="image1" />
              </div>
            </Grid>
          </Grid>
        </div>
      </Container>
    </section>
  );
}
